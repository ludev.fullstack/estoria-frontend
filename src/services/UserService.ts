import axios from "axios"

export default new class UserService{

    async getUser(){
        return await axios.get("http://localhost:8080/api/user/");
    }
}